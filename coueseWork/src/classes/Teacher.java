package classes;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;

public  class Teacher {
	private String password;
	private String login;
	public Teacher(String login, String password) throws Exception {
		this.password = password;
		this.login = login;
		ConnDb.Conn();
		 if (ConnDb.verificationLogin(this.login)){
			 ConnDb.CloseDB();
			 throw new Exception("Логин уже используется");
		 } else {
			 ConnDb.addTeacher(this.login,Methods.getHash(this.password));
			 ConnDb.CloseDB();
		 }
		
		
	}
	
	public static int login(String login,String password) throws ClassNotFoundException, SQLException, NoSuchAlgorithmException{
		ConnDb.Conn();
		int getLogin = ConnDb.login(login, password); 
		ConnDb.CloseDB();
		return getLogin;
	}
	
	public static  void addTest(int idTeacher, int maxTime, String title) throws ClassNotFoundException, SQLException {
		ConnDb.Conn();
		ConnDb.addTest(idTeacher, maxTime, title);
		ConnDb.CloseDB();
		
	}
	public static  void changeTest(int idTest, int maxTime, String title) throws ClassNotFoundException, SQLException {
		ConnDb.Conn();
		ConnDb.changeTest(idTest, maxTime, title);
		ConnDb.CloseDB();
		
	}
	
	public static ArrayList <Test> getTests (int idTeacher) throws ClassNotFoundException, SQLException {
		ArrayList <Test> getTests;
		ConnDb.Conn();
		getTests = ConnDb.getTests(idTeacher);
		ConnDb.CloseDB();
		return getTests;
	}
	
	public static ArrayList <Test> getTests (String title) throws ClassNotFoundException, SQLException {
		ArrayList <Test> getTests;
		ConnDb.Conn();
		getTests = ConnDb.getTests(title);
		ConnDb.CloseDB();
		return getTests;
	}
	
	public static ArrayList <Test> getTestsByLogin (String login) throws ClassNotFoundException, SQLException {
		ArrayList <Test> getTests;
		ConnDb.Conn();
		getTests = ConnDb.getTests(ConnDb.getIdByLogin(login));
		ConnDb.CloseDB();
		return getTests;
	}
	

}
