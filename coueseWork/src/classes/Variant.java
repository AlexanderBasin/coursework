package classes;

public class Variant {
	private String textVariant;
	private boolean answer;
	public Variant(String textVariant, boolean answer) {
		this.textVariant = textVariant;
		this.answer = answer;
	}
	public String getTextVariant() {
		return textVariant;
	}
	public boolean isAnswer() {
		return answer;
	}
	@Override
	public String toString() {
		return "Variant [textVariant=" + textVariant + ", answer=" + answer + "]";
	}
	
	
	
}
