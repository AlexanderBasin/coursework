package classes;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;









public class ConnDb {
	public static Connection conn;
	public static Statement statmt;
	
	
	
	
	// --------ПОДКЛЮЧЕНИЕ К БАЗЕ ДАННЫХ--------
	public static void Conn() throws ClassNotFoundException, SQLException 
	   {
		   conn = null;
		   Class.forName("org.sqlite.JDBC");
		   conn=  DriverManager.getConnection ("jdbc:sqlite::resource:http://localhost:8080/coueseWork/db/TestsBase.s3db");
		   System.out.println("База Подключена!");
		   statmt = conn.createStatement();
	   }
	
	
	
	
	public static void addTeacher(String login, String password) throws SQLException {

		   // Запрос с указанием мест для параметров в виде знака "?"
		   String sql = "INSERT INTO teachers (login, password) VALUES (?, ?)";
		   // Создание запроса. Переменная con - это объект типа Connection
		   PreparedStatement stmt = conn.prepareStatement(sql);
		   // Установка параметров
		   stmt.setString(1, login);
		   stmt.setString(2, password);
		   // Выполнение запроса
		   stmt.executeUpdate();
		   stmt.close();
	}
	
	public static boolean verificationLogin(String login) throws SQLException{
		
		
		String sql = "SELECT login FROM teachers WHERE login = ?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, login);
		ResultSet resSet = stmt.executeQuery();
		if (resSet.next()) {
			stmt.close();
			return true;
		}
		stmt.close();
		return false;
	}
	
	public static int login(String login , String password) throws SQLException, NoSuchAlgorithmException {
		String sql = "SELECT id_teacher,login, password FROM teachers WHERE login = ? AND password = ?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, login);
		stmt.setString(2, Methods.getHash(password));
		ResultSet resSet = stmt.executeQuery();
		int ret = 0;
		if (resSet.next()) {
			ret = resSet.getInt("id_teacher");
		}
		stmt.close();
		return ret;
	}
	
	public static int getIdByLogin(String login) throws SQLException {
		String sql = "SELECT id_teacher FROM teachers WHERE login = ?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, login);
		ResultSet resSet = stmt.executeQuery();
		int ret = 0;
		if (resSet.next()) {
			ret = resSet.getInt("id_teacher");
		}
		stmt.close();
		return ret;
	}
	
	public static void addTest(int idTeacher, int maxTime, String title) throws SQLException {
		
		// Запрос с указанием мест для параметров в виде знака "?"
		   String sql = "INSERT INTO tests (id_teacher, time, title_test) VALUES (?, ?,?)";
		   // Создание запроса. Переменная con - это объект типа Connection
		   PreparedStatement stmt = conn.prepareStatement(sql);
		   // Установка параметров
		   stmt.setInt(1, idTeacher);
		   stmt.setInt(2, maxTime);
		   stmt.setString(3, title);
		   // Выполнение запроса
		   stmt.executeUpdate();
		   stmt.close();
		   
	}
	
	public static void addQuestion(int idTest,String type,int score,String text,ArrayList <Variant> variants) throws SQLException, UnsupportedEncodingException {
		//добавляем вопрос
		String sql = "INSERT INTO questions (id_test, type, score,text_question) VALUES (?, ?,?,?)";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setInt(1,idTest);
		stmt.setString(2,type);
		stmt.setInt(3,score);
		stmt.setString(4,text);
		stmt.executeUpdate();
		stmt.close();
		//получаем id только что добавленного
		String sql2 = "SELECT MAX(id_question) FROM questions";
		PreparedStatement stmt2 = conn.prepareStatement(sql2);
		ResultSet resSet = stmt2.executeQuery();
		int maxId =0;
		if (resSet.next()) {
			maxId = resSet.getInt("MAX(id_question)");
		}
		stmt.close();
		//добавляем варианты
		
		String sql3 = "INSERT INTO variants (id_question, text_variant, answer) VALUES (?, ?,?)";
		for (Variant variant : variants) {
			PreparedStatement stmt3=null;
			stmt3 = conn.prepareStatement(sql3);
			stmt3.setInt(1,maxId);
			stmt3.setString(2,variant.getTextVariant());
			stmt3.setBoolean(3, variant.isAnswer());
			stmt3.executeUpdate();
			stmt3.close();
		}
		
	} 
	
	public static void changeTest(int idTest, int maxTime, String title) throws SQLException {
			// Запрос с указанием мест для параметров в виде знака "?"
			   String sql = "UPDATE tests SET  time = ?, title_test = ? WHERE id_test = ?  ";
			   // Создание запроса. Переменная con - это объект типа Connection
			   PreparedStatement stmt = conn.prepareStatement(sql);
			   // Установка параметров
			   stmt.setInt(1, maxTime);
			   stmt.setString(2, title);
			   stmt.setInt(3, idTest);
			   // Выполнение запроса
			   stmt.executeUpdate();
			   stmt.close();
			   
		}
	
	public static ArrayList <Test> getTests (int idTeacher) throws SQLException {
		String sql = "SELECT id_test, id_teacher,time,title_test FROM tests WHERE id_teacher = ? ";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setInt(1, idTeacher);
		ResultSet resSet = stmt.executeQuery();
		ArrayList <Test> result = new ArrayList<Test>();
		while(resSet.next()) {
			Test test = new Test(resSet.getInt("id_test"),resSet.getInt("time"),resSet.getString("title_test"));
			result.add(test);
		}
		stmt.close();
		return result;
	}
	
	public static ArrayList <Test> getTests (String title) throws SQLException {
		String sql = "SELECT id_test, id_teacher,time,title_test FROM tests WHERE title_test = ? ";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, title);
		ResultSet resSet = stmt.executeQuery();
		ArrayList <Test> result = new ArrayList<Test>();
		while(resSet.next()) {
			Test test = new Test(resSet.getInt("id_test"),resSet.getInt("time"),resSet.getString("title_test"));
			result.add(test);
		}
		stmt.close();
		return result;
	}
	
	
	
	public static ArrayList <Question> getQuestions (int idTest) throws SQLException {
		String sql = "SELECT id_question, id_test,type,score, text_question FROM questions WHERE id_test = ? ";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setInt(1, idTest);
		ResultSet resSet = stmt.executeQuery();
		ArrayList <Question> result = new ArrayList<Question>();
		while(resSet.next()) {
			Question question = new Question (resSet.getInt("id_question"),resSet.getString("type"),resSet.getInt("score"),resSet.getString("text_question"));
			result.add(question);
		}
		stmt.close();
		return result;
	}
	
	public static ArrayList <Variant> getVariants (int idQuestion) throws SQLException {
		String sql = "SELECT  text_variant, answer FROM variants WHERE id_question = ? ";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setInt(1, idQuestion);
		ResultSet resSet = stmt.executeQuery();
		ArrayList <Variant> result = new ArrayList<Variant>();
		while(resSet.next()) {
			Variant variant = new Variant (resSet.getString("text_variant"),resSet.getBoolean("answer"));
			result.add(variant);
		}
		stmt.close();
		return result;
	}
	
	public static void delQuestion (int idQuestion) throws SQLException {
		//удаляем вопрос
		String sql = "DELETE FROM questions WHERE id_question = ? ";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setInt(1, idQuestion);
		stmt.executeUpdate();
		stmt.close();
		//удаляем все его варианты ответов
		String sql2 = "DELETE FROM variants WHERE id_question = ? ";
		PreparedStatement stmt2 = conn.prepareStatement(sql2);
		stmt2.setInt(1, idQuestion);
		stmt2.executeUpdate();
		stmt2.close();
	}
	
	public static void delTest(int idTest) throws SQLException{
		//удаляем тест
		String sql = "DELETE FROM tests WHERE id_test = ? ";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setInt(1, idTest);
		stmt.executeUpdate();
		stmt.close();
		//удаляем все его вопросы и ответы
		ArrayList <Question> questions = ConnDb.getQuestions(idTest);
		for (Question question: questions) {
			ConnDb.delQuestion(question.getIdQuestion());
		}
		
	}
	
		// --------Закрытие--------
		public static void CloseDB() throws ClassNotFoundException, SQLException {
			conn.close();
			statmt.close();
			//
			
			System.out.println("Соединения закрыты");
		}
		
		
		

}

