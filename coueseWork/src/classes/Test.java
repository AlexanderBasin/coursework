package classes;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;

public class Test {
	private int idTest;
	private int maxTime;
	private String title;
	public Test(int idTest, int maxTime, String title) {
		this.idTest = idTest;
		this.maxTime = maxTime;
		this.title = title;
	}
	public int getIdTest() {
		return idTest;
	}
	public int getMaxTime() {
		return maxTime;
	}
	public String getTitle() {
		return title;
	}
	public static void addQuestion (int idTest,String type,int score,String text,ArrayList <Variant> variants) throws ClassNotFoundException, SQLException, UnsupportedEncodingException {
		ConnDb.Conn();
		ConnDb.addQuestion(idTest, type, score, text, variants);
		ConnDb.CloseDB();
	}
	
	public static ArrayList <Question> getQuestions (int idTest) throws ClassNotFoundException, SQLException {
		ArrayList <Question> getQuestions;
		ConnDb.Conn();
		getQuestions = ConnDb.getQuestions(idTest);
		ConnDb.CloseDB();
		return getQuestions;
	}
	
	public static void delTest (int idTest) throws SQLException, ClassNotFoundException {
		ConnDb.Conn();
		ConnDb.delTest(idTest);
		ConnDb.CloseDB();
	}
	
	
		

}

