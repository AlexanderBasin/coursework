package classes;

import java.sql.SQLException;
import java.util.ArrayList;

public class Question {
	private int idQuestion;
	private String type;
	private int score;
	private String text;
	public Question(int idQuestion, String type, int score, String text) {
		this.idQuestion = idQuestion;
		this.type = type;
		this.score = score;
		this.text = text;
	}
	public int getIdQuestion() {
		return idQuestion;
	}
	public String getType() {
		return type;
	}
	public int getScore() {
		return score;
	}
	public String getText() {
		return text;
	}
	
	public static void delQuestion (int idQuestion) throws SQLException, ClassNotFoundException {
		ConnDb.Conn();
		ConnDb.delQuestion(idQuestion);
		ConnDb.CloseDB();
	}
	
	public static ArrayList <Variant> getVariants (int idQuestion) throws ClassNotFoundException, SQLException {
		ArrayList <Variant> getVariants;
		ConnDb.Conn();
		getVariants = ConnDb.getVariants(idQuestion);
		ConnDb.CloseDB();
		return getVariants;
	}
	
	
	

}

