<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isErrorPage="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Ошибка</title>
</head>
<body>
	<%@ include file="head.jsp" %>
	<div id="content">
 	<div id ="error">
	 	<h1>Opps...</h1>
		<%=exception.getMessage() %>
	</div>
	</div>
</body>
</html>