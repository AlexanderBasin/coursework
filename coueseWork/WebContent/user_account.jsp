<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="classes.Teacher,classes.Test,classes.ConnDb,classes.Methods,java.util.ArrayList,classes.Variant,classes.Question" 
    errorPage="error.jsp" %>
<%
	request.setCharacterEncoding("UTF-8");
	HttpSession ses = request.getSession(); 
	if (request.getMethod().equals("POST")) {
		String login = request.getParameter("login");
		String password = request.getParameter("password");
		
		if (ses.getAttribute("id_teacher")==null) {
			int id_teacher = Teacher.login(login, password);
			if (id_teacher!=0){
				ses.setAttribute("id_teacher", id_teacher);
				ses.setMaxInactiveInterval(600);
			} else {
				throw new Exception("Неверный логин или пароль");
			}
		} 
		
		if (request.getParameter("exit")!=null){
			ses.setAttribute("id_teacher", null);
			response.sendRedirect("index.jsp");
			
			
			
		}
		
		if (request.getParameter("add_test")!=null){
			String titleTest = request.getParameter("title_test");
			int time = Integer.parseInt(request.getParameter("time"));
			Teacher.addTest( Integer.parseInt(ses.getAttribute("id_teacher").toString()) ,time,titleTest);
		
		}
		
		if (request.getParameter("change_test")!=null){
			String titleTest = request.getParameter("new_title_test");
			int time = Integer.parseInt(request.getParameter("new_time"));
			int idTest = Integer.parseInt(request.getParameter("id_test"));
			Teacher.changeTest( idTest ,time,titleTest);
		
		}
		
		if (request.getParameter("add_question")!=null){
			String textQuestion = request.getParameter("text_question");
			int score = Integer.parseInt( request.getParameter("score"));
			String type = request.getParameter("type");
			int countVariants = Integer.parseInt(request.getParameter("count_variants"));
			int idTest = Integer.parseInt(request.getParameter("id_test"));
			ArrayList <Variant> variants = new ArrayList<Variant>();
			for (int i=1;i<=countVariants;i++) {
				boolean answer =false;
				if (request.getParameter("answer"+i).equals("yes")) answer = true;
				variants.add(new Variant(request.getParameter("text_variant"+i),answer));
			}
			Test.addQuestion(idTest, type, score, textQuestion, variants);
		}
		
		if ( request.getParameter("del_question")!=null){
			int idQuestion = Integer.parseInt( request.getParameter("del_question"));
			Question.delQuestion(idQuestion);
		}
		
		if ( request.getParameter("del_test")!=null){
			int idTest = Integer.parseInt( request.getParameter("del_test"));
			Test.delTest(idTest);
		}
		
	} else {
		throw new Exception("Такой страницы не существует");
	}
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.1.1.js"></script>
<script type="text/javascript">
	var sesTime = <%=ses.getMaxInactiveInterval()%>;
	$(function(){
		$("#timer").html(sesTime);
		setInterval("sesTime--; $('#timer').html(sesTime); if(sesTime==0)document.location.href='index.jsp'; ", 1000);
	});
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/user_account.js"></script>

<title>Учетная запись пользователя</title>
</head>
<body>
	<%@ include file="head.jsp" %>
	<div id="content">
	<div><%=ses.getAttribute("id_teacher") %></div>
	<div id="timer"></div>
  <form action="" method="post">
  	<ul style="display:inline-block">
	  	<li><button name="exit" value="exit">Выход  </button></li>
	  	<li><button   onclick="addTest()" type="button">Добавить тест  </button></li>
  	</ul><br>
  	<table id="add_test" class="hidden">
  	   <tr>
  	   		<td><span>Название</span></td>
  	   		<td><input type="text" name="title_test"></td> 
  	   </tr>
  	   <tr>
  	   		<td><span>Время выполнения</span></td>
  	   		<td><input type="text" name="time"></td> 
  	   </tr>
  	   <tr> <td><button name="add_test" value="add_test">Добавить  </button> </td></tr>
  	</table>
  	<ul id = "tests">
  		
  		<% 
  		if (ses.getAttribute("id_teacher")!=null) {
  			for( Test test : Teacher.getTests(Integer.parseInt(ses.getAttribute("id_teacher").toString()) ) )  {
  		%>
  		<li class = "test"  >
  			<label title="время выполнения <%=test.getMaxTime() %>" onclick="showQuestions(this)"><span><%=test.getTitle() %> </span></label>
  			<button type="button"  class="btn_change_test" onclick="changeTest(this,<%=test.getIdTest()%>,<%=test.getMaxTime()%>,'<%=test.getTitle()%>')">~</button> 
 			<button type="button" onclick="addQuestion(this,<%=test.getIdTest()%>)">+</button> 
 			<button name="del_test" value=<%=test.getIdTest() %> >-</button> 
  			<ul class="questions hidden">
  				<%
  					for (Question question:Test.getQuestions(test.getIdTest())) {
  				%>
  				<li class="question">
  					<%
  						String type ="тип неизвестен";
  						if (question.getType().equals("c")) type = "множественный выбор";
  						if (question.getType().equals("r")) type = "одиночный выбор";
  					%>
  					<label onclick="showQuestions(this)" style="color:green" title="<%=question.getScore() %> очков, <%=type %>"     ><span><%=question.getText() %></span></label>
  					<button style="color:green" name="del_question" value=<%=question.getIdQuestion() %>>x</button> 
  					<ul class="variants hidden">
  						<%for (Variant variant:Question.getVariants(question.getIdQuestion())) {%>
  							<li class="variant"> 
  								<span ><%=variant.getTextVariant() %></span>
  								<%if (variant.isAnswer()) {%>
  									<input style="" disabled type ="checkbox" class="as" checked>
  								<% }else{ %>
  									<input disabled type ="checkbox" class="as">
  								<%} %>
  							</li>
  						<%} %>
  					</ul>
  				</li>
  			<% } %>	
  			</ul>
  		</li>
  		
  		<%}}  %>
  	</ul>
  </form>
 </div> 
</body>
</html>