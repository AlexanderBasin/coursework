<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="classes.Teacher,java.util.ArrayList,classes.Test,classes.Question,classes.Variant" %>
<%
	int idTest = Integer.parseInt(request.getParameter("id_test"));
	ArrayList <Question> questions = Test.getQuestions(idTest);	
	
%>  

  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.1.1.js"></script>
<script type="text/javascript">
	var questions = [];
	<%for (Question question:questions) {%>
		var question = {};
		question.type ='<%=question.getType()%>';
		question.score = <%=question.getScore()%>;
		question.text ='<%=question.getText()%>';
		question.variants = [];
		<%for (Variant variant:Question.getVariants(question.getIdQuestion())) {%>
			var variant = {};
			variant.text = '<%=variant.getTextVariant()%>';
			variant.answer = <%=variant.isAnswer()%>;
			question.variants.push(variant);
		<%}%>
		questions.push(question)
	<%}%>
	
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/start_test.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Начало теста</title>
</head>
<body>
	<%@ include file="head.jsp" %>
	<div id="content">
	<div id="questions" style="width:400px;height:400px;border:1px solid red">
		
	</div>
	<button  onclick="number--;showQuestion(number);"> prev </button>
	<button  onclick="number++;showQuestion(number);">next</button> <br>
	<button  onclick="sendResult()">Сдать тест</button> <br>
	</div>
</body>
</html>