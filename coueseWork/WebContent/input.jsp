<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<% 
	request.getSession().setAttribute("id_teacher", null);
	
%>      
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Вход</title>
</head>
<body>
	<%@ include file="head.jsp" %>
	<div id="content">
	<form action="user_account.jsp" method = "post">
		<table>
			<tr>
				<td><span>Логин</span></td>
				<td><input type="text" name="login"></td>
			</tr>
			<tr>
				<td><span>Пароль</span></td>
				<td><input type="password" name="password"></td>
			</tr>
			<tr>
				<td><input type="submit" value="Вход"></td>
			</tr>
		</table>
	</form>
	</div>
</body>
</html>