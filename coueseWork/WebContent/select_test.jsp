<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="classes.Teacher,java.util.ArrayList,classes.Test" %>
 <%
 	ArrayList <Test> tests = null;
 	if (request.getMethod().equals("POST")) { 
 		String searchValue = request.getParameter("search_value");
 		if (request.getParameter("search_how").equals("login")){
 			tests = Teacher.getTestsByLogin(searchValue);
 		}
 		
		if (request.getParameter("search_how").equals("title_test")){
			tests = Teacher.getTests(searchValue);
 		}
 	}
 %>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Прохождение теста</title>
</head>
<body>
	<%@ include file="head.jsp" %>
	<div id="content">
	<form action="" method="POST">
		<table>
			<tr>
				<td><span>Текст поиска</span></td>
				<td><input type="text" name="search_value"></td>
			</tr>
			<tr>
				<td><span>Искать по</span></td>
				<td>
					<select name="search_how" >
						<option value="login">Логин учителя</option>
						<option value="title_test">Название теста</option>
					</select>
				</td>
			</tr>
			<tr>
				<td><input type="submit" value="Поехали"></td>
			</tr>
		</table>
	</form>
	<div id="tests">
		<ul>
		<%if(tests!=null) { %>
			
			<% for (Test test:tests) {%>
			<li>
				<a href="start_test.jsp?id_test=<%=test.getIdTest() %>"><%=test.getTitle() %></a>
			</li>
		<%} }%>
		</ul>
	</div>
</div>	
</body>
</html>