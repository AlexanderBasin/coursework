/**
 * 
 */
function rawQuestions (){ 
var blockQuestions ="";
		for (var i=0;i<questions.length;i++){
			blockQuestions+="<div class='question' style='display:none'><span>" +questions[i].text+"</span>";
				var blockVariants="";
				for (var j = 0; j < questions[i].variants.length;j++ ) {
					blockVariants += "<div class='variant'>";
					if (questions[i].type =='c')
						blockVariants+="<input type = 'checkbox' name='result"+(i+1)+"' value="+(j+1)+">";
						else blockVariants+="<input type = 'radio' name='result"+(i+1)+"' value="+(j+1)+">";
					blockVariants+=	"<span>"+questions[i].variants[j].text+ "</span></div>";
				}
			blockQuestions+=blockVariants;
			blockQuestions+="</div>"
		}
		$("#questions").html(blockQuestions);
}		
		var number=1;
		$(function(){
			rawQuestions ();
			showQuestion(1);
		});
		function showQuestion(n){
			$('.question').css('display','none');
			$('.question').eq(n-1).css('display','block');
		}
	
		function sendResult(){
			var sumResults=0;
			var sumScore=0;
			for (var i=0;i<questions.length;i++){
				if (questions[i].type =='r') {
					var value = $("input[name=result"+(i+1)+"]:checked").val()-1;
					if (questions[i].variants[value].answer===true) {
						sumResults+=questions[i].score;
					}
				}
				
				if (questions[i].type =='c'){
					var inputs = $("input[name=result"+(i+1)+"]:checked");
					values=[];
					countAnswers=inputs.length;
					for (var j=0;j<countAnswers;j++){ 
						values[j] = inputs.eq(j).val()-1; 
					}
					var countRightVariants = 0;
					var countSelectRightVariants=0;
					for (var k=0;k<questions[i].variants.length;k++) {
						if (questions[i].variants[k].answer===true) {
							countRightVariants++;
							if (values.includes(k)) {
								countSelectRightVariants++;
							}
						}
						
						
					}
						
					if(countAnswers===countRightVariants && countRightVariants===countSelectRightVariants) {
						sumResults+=questions[i].score;
					}
					
					if((countRightVariants===countSelectRightVariants && (countAnswers-countRightVariants)===1) || (countAnswers===countSelectRightVariants &&  (countRightVariants-countAnswers)===1            )){
						sumResults+=questions[i].score*0.2;
					}
						
					

				}
				sumScore+=questions[i].score;
			}
			alert((sumResults/sumScore)*100+' %');
		}